<?php
/*
Plugin Name: AE Require Login
Version: 1.0
Description: Require login plugin allowd your blog require login. Supported multi-site (mu).
Author: Kenichi Inoue (Artisan Edge)
Author URI: http://artisanedge.jp
Plugin URI: http://git.artisanedge.jp/ae-require-login
Text Domain: ae-require-login
Domain Path: /languages
*/

add_action( 'init', 'require_login' );

function require_login() {
	if ( (
		 ! is_user_logged_in() &&
		 ! preg_match( '/^(wp-login\.php|async-upload\.php|token-login\.php|token-logout\.php|xmlrpc\.php|sms_wave\.png)/', basename( $_SERVER['REQUEST_URI'] ) ) &&
		 ! preg_match( '/import/', $_SERVER['REQUEST_URI'] ) &&
		 ! preg_match( '/admin/', $_SERVER['REQUEST_URI'] ) && 
		 ! preg_match( '/.*-auth.*/', $_SERVER['REQUEST_URI'] ) &&
		 ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) &&
		 ! ( defined( 'DOING_CRON' ) && DOING_CRON )
		 )
		 || ( is_user_logged_in() && ! current_user_can( 'read' ) ) ) {
		if ( ! is_user_logged_in() ) {
			switch_to_blog( 1 );
		} else {
			wp_logout();
		}
		auth_redirect();
	}
}
